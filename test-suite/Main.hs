import qualified Test.Tasty

import Control.Monad (forM_)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader.Class (asks)
import qualified Data.Map.Strict as Map
import Data.Maybe (isJust)
import Orvis.Adapter.Intake (intake_)
import qualified Orvis.Mailbox as Mailbox
import qualified Orvis.Metrics as Metrics
import qualified Orvis.Port as Port
import qualified Orvis.Port.Program.Spawner as Port.Program.Spawner
import qualified Orvis.Robot as Robot
import qualified Orvis.Robot.Program as Robot.Program
import Orvis.Topology (metrics, topology)
import Test.Tasty.Hspec (Spec, it, parallel, shouldBe, shouldSatisfy, testSpec)

main :: IO ()
main = do
  test <- testSpec "A sample Orvis topology" spec
  Test.Tasty.defaultMain test

spec :: Spec
spec =
  let dummyProgram =
        Robot.Program.Program
          { Robot.Program.initialize = const ((), Robot.Program.NoOp)
          , Robot.Program.update =
              const . const . const $ ((), Robot.Program.NoOp)
          , Robot.Program.represent = const ()
          }
      dummyRobot mailbox =
        Robot.Robot
          { Robot.program = dummyProgram
          , Robot.readsFrom = [intake_ mailbox]
          , Robot.writesTo = []
          , Robot.rendersTo = []
          }
   in parallel $ do
        it "has correct metrics for one mailbox" $
          topology $ do
            mailbox <- Mailbox.spawn "mailbox"
            tracker <- asks metrics
            mailboxMetricMap <- liftIO $ Metrics.getMailboxes tracker
            liftIO $ Map.size mailboxMetricMap `shouldBe` 1
            liftIO $
              Map.lookup (Mailbox.mailboxId mailbox) mailboxMetricMap `shouldSatisfy`
              isJust
        it "has correct metrics for multiple mailboxes" $
          topology $ do
            forM_ [1 :: Int .. 1500] $ const $ Mailbox.spawn "mailbox"
            tracker <- asks metrics
            mailboxMetricMap <- liftIO $ Metrics.getMailboxes tracker
            liftIO $ Map.size mailboxMetricMap `shouldBe` 1500
        it "has correct metrics for one robot" $
          topology $ do
            mailbox <- Mailbox.spawn "mailbox"
            Robot.spawn "robot" () $ dummyRobot mailbox
            tracker <- asks metrics
            robotMetricMap <- liftIO $ Metrics.getRobots tracker
            liftIO $ Map.size robotMetricMap `shouldBe` 1
        it "has correct metrics for multiple robots" $
          topology $ do
            mailbox <- Mailbox.spawn "mailbox"
            forM_ [1 :: Int .. 1000] $
              const $ Robot.spawn "robot" () (dummyRobot mailbox)
            tracker <- asks metrics
            robotMetricMap <- liftIO $ Metrics.getRobots tracker
            liftIO $ Map.size robotMetricMap `shouldBe` 1000
        it "has correct metrics for one port" $
          topology $ do
            mailbox <- Mailbox.spawn "mailbox"
            Port.spawn
              "port"
              ()
              Port.Port
                { Port.program =
                    Port.Program.Spawner.spawner $
                    const ("robot", (), dummyRobot mailbox)
                , Port.subscriptionPoints = []
                , Port.broadcastPoints = []
                }
            tracker <- asks metrics
            portMetricMap <- liftIO $ Metrics.getPorts tracker
            liftIO $ Map.size portMetricMap `shouldBe` 1
        it "has correct metrics for multiple ports" $
          topology $ do
            mailbox <- Mailbox.spawn "mailbox"
            forM_ [1 :: Int .. 500] $
              const $
              Port.spawn
                "port"
                ()
                Port.Port
                  { Port.program =
                      Port.Program.Spawner.spawner $
                      const ("robot", (), dummyRobot mailbox)
                  , Port.subscriptionPoints = [intake_ mailbox]
                  , Port.broadcastPoints = []
                  }
            tracker <- asks metrics
            portMetricMap <- liftIO $ Metrics.getPorts tracker
            liftIO $ Map.size portMetricMap `shouldBe` 500
