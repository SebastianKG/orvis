module Orvis.Flags
  ( Flags(..)
  ) where

import Orvis.Unique (Id)

data Flags custom =
  Flags Id
        custom
