{-# LANGUAGE NamedFieldPuns #-}

module Orvis.Port
  ( Port(..)
  , spawn
  ) where

import Orvis.Adapter.Intake (Intake)
import qualified Orvis.Adapter.Intake as Intake
import Orvis.Adapter.Outflow (Outflow)
import qualified Orvis.Adapter.Outflow as Outflow
import Orvis.Flags (Flags(..))
import qualified Orvis.Metrics as Metrics
import Orvis.Port.Program (CleanUp(..), Program(..), fromRawSubscription)
import Orvis.Topology (Topology, topologyWith)
import qualified Orvis.Topology as Topology
import qualified Orvis.Unique as Unique

import Control.Concurrent (forkFinally)
import Control.Monad (void)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader.Class (ask)

data Port flags initialState input output = Port
  { program :: Program flags initialState input output
  , subscriptionPoints :: [Intake input]
  , broadcastPoints :: [Outflow output]
  }

spawn ::
     String
  -> customFlags
  -> Port customFlags initialState input output
  -> Topology ()
spawn name customFlags Port { program = Program {initialize, run}
                            , subscriptionPoints
                            , broadcastPoints
                            } = do
  tools@Topology.Tools { Topology.counter = counter
                       , Topology.metrics = metricsTracker
                       } <- ask
  myId <- liftIO $ Unique.getNext counter
  (subscription, killSubscription) <- Intake.subscribe myId subscriptionPoints
  let broadcaster = Outflow.merge broadcastPoints
      inboxIds = Intake.mailboxId <$> subscriptionPoints
      outboxIds = Outflow.mailboxId <$> broadcastPoints
      flags = Flags myId customFlags
  void $
    liftIO $ do
      Metrics.addPort
        metricsTracker
        myId
        Metrics.Port
          { Metrics.portName = name
          , Metrics.subscriptionPoints = inboxIds
          , Metrics.broadcastPoints = outboxIds
          }
      topologyWith tools $ do
        (initialState, CleanUp {doCleanUp}) <- initialize flags
        liftIO $
          forkFinally
            (topologyWith tools $
             run
               flags
               initialState
               (fromRawSubscription subscription)
               broadcaster)
            (const $ do
               topologyWith tools doCleanUp
               killSubscription
               Metrics.removePort metricsTracker myId)
