{-# LANGUAGE NamedFieldPuns #-}

module Orvis.Robot
  ( Robot(..)
  , spawn
  , spawnWithId
  ) where

import Orvis.Adapter.Intake (Intake, Subscription(..))
import qualified Orvis.Adapter.Intake as Intake
import Orvis.Adapter.Outflow (Broadcaster(..), Outflow(..))
import qualified Orvis.Adapter.Outflow as Outflow
import Orvis.Flags (Flags(..))
import Orvis.Mailbox (Message(..))
import qualified Orvis.Metrics as Metrics
import Orvis.Robot.Program (Cmd(..), Program)
import qualified Orvis.Robot.Program as Program
import Orvis.Topology (Topology)
import qualified Orvis.Topology as Topology
import Orvis.Unique (Id)
import qualified Orvis.Unique as Unique

import Control.Concurrent (forkIO)
import Control.Monad (void)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader.Class (asks)

data Robot flags model input output representation = Robot
  { program :: Program flags model input output representation
  , readsFrom :: [Intake input]
  , writesTo :: [Outflow output]
  , rendersTo :: [Outflow representation]
  }

spawnWithId ::
     String
  -> Flags flags
  -> Robot flags model input output representation
  -> Topology ()
spawnWithId name flags Robot {program, readsFrom, writesTo, rendersTo} = do
  let Flags givenId _ = flags
      inboxIds = Intake.mailboxId <$> readsFrom
      outBroadcaster = Outflow.merge writesTo
      outboxIds = Outflow.mailboxId <$> writesTo
      renderBroadcaster = Outflow.merge rendersTo
      renderboxIds = Outflow.mailboxId <$> rendersTo
  (subscription, killSubscription) <- Intake.subscribe givenId readsFrom
  metricsTracker <- asks Topology.metrics
  liftIO $ do
    Metrics.addRobot
      metricsTracker
      givenId
      Metrics.Robot
        { Metrics.robotName = name
        , Metrics.readsFrom = inboxIds
        , Metrics.writesTo = outboxIds
        , Metrics.rendersTo = renderboxIds
        }
    spawnCore
      (killSubscription >> Metrics.removeRobot metricsTracker givenId)
      flags
      program
      subscription
      outBroadcaster
      renderBroadcaster

spawn ::
     String
  -> flags
  -> Robot flags model input output representation
  -> Topology ()
spawn name customFlags robot = do
  c <- asks Topology.counter
  myId <- liftIO . Unique.getNext $ c
  let flags = Flags myId customFlags
  spawnWithId name flags robot

data ExecutionStep model input output representation = Step
  { state :: model
  , update :: input -> model -> (model, Cmd output)
  , represent :: model -> representation
  }

type Finalizer = IO ()

spawnCore ::
     Finalizer
  -> Flags flags
  -> Program flags model input output representation
  -> Subscription input
  -> Broadcaster output
  -> Broadcaster representation
  -> IO ()
spawnCore onDeath flags program subscription outBroadcaster renderBroadcaster =
  let Flags myId _ = flags
      Broadcaster {send = sendOutput} = outBroadcaster
      Broadcaster {send = sendRender} = renderBroadcaster
      (initialStep@Step {state = state', represent = represent'}, firstCommand) =
        firstStep flags program
      representation = represent' state'
      go = do
        sendRender myId representation
        tickLoop
          onDeath
          myId
          initialStep
          subscription
          outBroadcaster
          renderBroadcaster
   in void $
      forkIO $
      case firstCommand of
        NoOp -> go
        Cmd msg -> do
          sendOutput myId msg
          go
        Batch msgList -> do
          mapM_ (sendOutput myId) msgList
          go
        Die msgList -> do
          mapM_ (sendOutput myId) msgList
          sendRender myId representation
          onDeath

firstStep ::
     Flags flags
  -> Program flags model input output representation
  -> (ExecutionStep model input output representation, Cmd output)
firstStep flags Program.Program { Program.initialize = initialize'
                                , Program.update = update'
                                , Program.represent = represent'
                                } =
  let Flags _ customFlags = flags
      (firstState, firstCommand) = initialize' customFlags
   in ( Step
          { state = firstState
          , update = update' customFlags
          , represent = represent'
          }
      , firstCommand)

tickLoop ::
     Finalizer
  -> Id
  -> ExecutionStep model input output representation
  -> Subscription input
  -> Broadcaster output
  -> Broadcaster representation
  -> IO ()
tickLoop _ _ _ NotSubscribed _ _ = putStrLn "Not subscribed"
tickLoop onDeath i executionStep s@Subscription {next} os rs = do
  Message {message} <- next
  nextStep <- tick i message executionStep os rs
  case nextStep of
    Nothing -> onDeath
    Just step -> tickLoop onDeath i step s os rs

tick ::
     Id
  -> input
  -> ExecutionStep model input output representation
  -> Broadcaster output
  -> Broadcaster representation
  -> IO (Maybe (ExecutionStep model input output representation))
tick myId input executionStep outBroadcast renderBroadcast =
  let Broadcaster {send = output} = outBroadcast
      Broadcaster {send = render} = renderBroadcast
      step@Step {state = state', update = update', represent = represent'} =
        executionStep
      (newState, outMessage) = update' input state'
      representation = represent' newState
      continue = return $ Just $ step {state = newState}
      die = return Nothing
   in do render myId representation
         case outMessage of
           NoOp -> continue
           Cmd msg -> do
             output myId msg
             continue
           Batch msgList -> do
             output myId `mapM_` msgList
             continue
           Die msgList -> do
             output myId `mapM_` msgList
             die
