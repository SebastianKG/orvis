{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Orvis.Topology
  ( Topology
  , Tools(..)
  , topology
  , topologyWith
  ) where

import Orvis.Metrics (Tracker)
import qualified Orvis.Metrics as Metrics
import Orvis.Unique (Counter)
import qualified Orvis.Unique as Unique

import Control.Monad.IO.Class (MonadIO(..))
import Control.Monad.Reader.Class (MonadReader(..))
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Reader (ReaderT, runReaderT)

data Tools = Tools
  { counter :: Counter
  , metrics :: Tracker
  }

newtype Topology a = Topology
  { run :: ReaderT Tools IO a
  } deriving (Functor, Applicative, Monad)

instance MonadIO Topology where
  liftIO = Topology . lift

instance MonadReader Tools Topology where
  ask = Topology ask
  local f = Topology . local f . run

topology :: Topology a -> IO a
topology action = do
  tools <- Tools <$> Unique.spawnCounter <*> Metrics.spawnTracker
  topologyWith tools action

topologyWith :: Tools -> Topology a -> IO a
topologyWith tools action = runReaderT (run action) tools
