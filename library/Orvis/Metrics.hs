{-# LANGUAGE NamedFieldPuns #-}

module Orvis.Metrics
  ( Mailbox(..)
  , Port(..)
  , Robot(..)
  , Tracker
  , spawnTracker
  , addMailbox
  , addPort
  , addRobot
  , getMailboxes
  , getPorts
  , getRobots
  , removePort
  , removeRobot
  ) where

import Orvis.Unique (Id)

import Control.Concurrent.STM.TVar (TVar, modifyTVar', newTVar, readTVarIO)
import Control.Monad.STM (atomically)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map

spawnTracker :: IO Tracker
spawnTracker =
  atomically $
  Tracker <$> newTVar Map.empty <*> newTVar Map.empty <*> newTVar Map.empty

data Tracker = Tracker
  { mailboxMetrics :: TVar (Map Id Mailbox)
  , portMetrics :: TVar (Map Id Port)
  , robotMetrics :: TVar (Map Id Robot)
  }

newtype Mailbox = Mailbox
  { mailboxName :: String
  } deriving (Show)

data Port = Port
  { portName :: String
  , subscriptionPoints :: [Id]
  , broadcastPoints :: [Id]
  } deriving (Show)

data Robot = Robot
  { robotName :: String
  , readsFrom :: [Id]
  , writesTo :: [Id]
  , rendersTo :: [Id]
  } deriving (Show)

addMailbox :: Tracker -> Id -> Mailbox -> IO ()
addMailbox Tracker {mailboxMetrics} mailboxId newMailbox =
  atomically $
  modifyTVar' mailboxMetrics $ \metrics ->
    Map.insert mailboxId newMailbox metrics

addPort :: Tracker -> Id -> Port -> IO ()
addPort Tracker {portMetrics} portId newPort =
  atomically $
  modifyTVar' portMetrics $ \metrics -> Map.insert portId newPort metrics

removePort :: Tracker -> Id -> IO ()
removePort Tracker {portMetrics} portId =
  atomically $ modifyTVar' portMetrics $ \metrics -> Map.delete portId metrics

addRobot :: Tracker -> Id -> Robot -> IO ()
addRobot Tracker {robotMetrics} robotId newRobot =
  atomically $
  modifyTVar' robotMetrics $ \metrics -> Map.insert robotId newRobot metrics

removeRobot :: Tracker -> Id -> IO ()
removeRobot Tracker {robotMetrics} robotId =
  atomically $ modifyTVar' robotMetrics $ \metrics -> Map.delete robotId metrics

getMailboxes :: Tracker -> IO (Map Id Mailbox)
getMailboxes = readTVarIO . mailboxMetrics

getPorts :: Tracker -> IO (Map Id Port)
getPorts = readTVarIO . portMetrics

getRobots :: Tracker -> IO (Map Id Robot)
getRobots = readTVarIO . robotMetrics
