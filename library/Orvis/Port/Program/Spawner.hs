{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NamedFieldPuns #-}

module Orvis.Port.Program.Spawner
  ( Input(..)
  , Output(..)
  , spawner
  ) where

import Orvis.Adapter.Outflow (Broadcaster(..))
import Orvis.Flags (Flags(..))
import Orvis.Port.Program (CleanUp(..), Program(..), Subscription(..))
import Orvis.Robot (Robot)
import qualified Orvis.Robot as Robot
import Orvis.Topology (counter, topologyWith)
import qualified Orvis.Topology as Topology
import Orvis.Unique (Id, getNext)

import Control.Monad (forever)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader.Class (ask)

newtype Output =
  Spawned Id

data Input input
  = Spawn input
  | SpawnWithId Id
                input

spawner ::
     (input -> (String, flags, Robot flags model i o r))
  -> Program () () (Input input) Output
spawner inputToRobot =
  Program
    { initialize = \_ -> return ((), CleanUp {doCleanUp = return ()})
    , run =
        \flags () subscription Broadcaster {send} ->
          case subscription of
            NotSubscribed -> return ()
            Subscription {next} -> do
              tools@Topology.Tools {counter = idCounter} <- ask
              let Flags spawnerId _ = flags
              liftIO $
                forever $ do
                  msg <- next
                  (name, robotFlags, robotDescription) <-
                    spawnRequestToRobot idCounter msg
                  topologyWith tools $
                    Robot.spawnWithId name robotFlags robotDescription
                  let Flags robotId _ = robotFlags
                  send spawnerId $ Spawned robotId
    }
  where
    spawnRequestToRobot idCounter request = do
      (robotId, input) <-
        case request of
          Spawn input -> do
            robotId <- getNext idCounter
            return (robotId, input)
          SpawnWithId robotId input -> pure (robotId, input)
      let (name, customFlags, robot) = inputToRobot input
      return (name, Flags robotId customFlags, robot)
