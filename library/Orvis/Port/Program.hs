{-# LANGUAGE NamedFieldPuns #-}

module Orvis.Port.Program
  ( Program(..)
  , CleanUp(..)
  , Subscription(..)
  , fromRawSubscription
  ) where

import qualified Orvis.Adapter.Intake as Intake
import Orvis.Adapter.Outflow (Broadcaster)
import Orvis.Flags (Flags(..))
import Orvis.Mailbox (Message(..))
import Orvis.Topology (Topology)

data Program flags initialState input output = Program
  { initialize :: Flags flags -> Topology (initialState, CleanUp)
  , run :: Flags flags -> initialState -> Subscription input -> Broadcaster output -> Topology ()
  }

newtype CleanUp = CleanUp
  { doCleanUp :: Topology ()
  }

data Subscription msg
  = NotSubscribed
  | Subscription { next :: IO msg }

fromRawSubscription :: Intake.Subscription msg -> Subscription msg
fromRawSubscription subscription =
  case subscription of
    Intake.NotSubscribed -> NotSubscribed
    Intake.Subscription {Intake.next = rawNext} ->
      Subscription
        { next =
            do Message {message} <- rawNext
               return message
        }
