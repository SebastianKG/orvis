module Orvis.Robot.Program
  ( Cmd(..)
  , Program(..)
  ) where

data Program flags model input output representation = Program
  { initialize :: flags -> (model, Cmd output)
  , update :: flags -> input -> model -> (model, Cmd output)
  , represent :: model -> representation
  }

data Cmd msg
  = NoOp
  | Cmd msg
  | Batch [msg]
  | Die [msg]

instance Monoid (Cmd msg) where
  mempty = NoOp
  -- with NoOp 
  NoOp `mappend` msg = msg
  msg `mappend` NoOp = msg
  -- with Cmd
  Cmd msg1 `mappend` Cmd msg2 = Batch [msg1, msg2]
  -- with Batch & Cmd
  Cmd msg `mappend` Batch list = Batch $ msg : list
  Batch list `mappend` Cmd msg = Batch $ list `mappend` [msg]
  Batch l1 `mappend` Batch l2 = Batch $ l1 `mappend` l2
  -- with Die & Batch & Cmd
  Cmd msg `mappend` Die list = Die $ msg : list
  Die list `mappend` Cmd msg = Die $ list `mappend` [msg]
  Die l1 `mappend` Batch l2 = Die $ l1 `mappend` l2
  Batch l1 `mappend` Die l2 = Die $ l1 `mappend` l2
  Die l1 `mappend` Die l2 = Die $ l1 `mappend` l2
