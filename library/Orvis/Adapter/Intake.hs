{-# LANGUAGE ExistentialQuantification, NamedFieldPuns #-}

module Orvis.Adapter.Intake
  ( Intake
  , Subscription(..)
  , intake
  , intake_
  , mailboxId
  , subscribe
  ) where

import Orvis.Mailbox (Mailbox, Message(..))
import qualified Orvis.Mailbox as Mailbox
import Orvis.Topology (Topology)
import Orvis.Unique (Id)

import Control.Concurrent (ThreadId, forkIO, killThread)
import Control.Monad (forever, mapM, mapM_, void)
import Control.Monad.IO.Class (liftIO)

type ReceiverId = Id

data Intake to =
  forall from. Intake (Mailbox from)
                      (ReceiverId -> Message from -> Maybe to)

intake :: Mailbox from -> (ReceiverId -> Message from -> Maybe to) -> Intake to
intake = Intake

intake_ :: Mailbox to -> Intake to
intake_ mailbox = Intake mailbox $ \_ Message {message} -> Just message

mailboxId :: Intake msg -> Id
mailboxId (Intake mailbox _) = Mailbox.mailboxId mailbox

data Subscription msg
  = NotSubscribed
  | Subscription { next :: IO (Message msg) }

subscribeToOne :: ReceiverId -> Intake to -> IO (Subscription to)
subscribeToOne receiverId (Intake mailbox adapt) = do
  out <- Mailbox.subscribe mailbox
  return $
    Subscription $ do
      let acquireMessage = do
            msg@Message {identity} <- Mailbox.next out
            case adapt receiverId msg of
              Just adaptedMsg ->
                return Message {identity = identity, message = adaptedMsg}
              Nothing -> acquireMessage
      acquireMessage

type Finalizer = IO ()

subscribe :: ReceiverId -> [Intake to] -> Topology (Subscription to, Finalizer)
subscribe receiverId intakeCollection =
  case intakeCollection of
    [] -> return (NotSubscribed, return ())
    [singleIntake] ->
      let noCleanup = return ()
       in do subscription <- liftIO $ subscribeToOne receiverId singleIntake
             return (subscription, noCleanup)
    intakes -> do
      interstitial <- Mailbox.spawnHidden
      let interstitialInbox = Mailbox.open interstitial
      threads <-
        liftIO $ redirectMail receiverId interstitialInbox `mapM` intakes
      let cleanup = killThread `mapM_` threads
          interstitialIntake = intake_ interstitial
      subscription <- liftIO $ subscribeToOne receiverId interstitialIntake
      return (subscription, cleanup)

redirectMail :: ReceiverId -> Mailbox.In msg -> Intake msg -> IO ThreadId
redirectMail receiverId interstitial singleIntake = do
  Subscription {next} <- subscribeToOne receiverId singleIntake
  forkIO $
    void $
    forever $ do
      msg <- next
      interstitial `Mailbox.send` msg
