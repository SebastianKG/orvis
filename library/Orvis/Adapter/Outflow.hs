{-# LANGUAGE ExistentialQuantification #-}

module Orvis.Adapter.Outflow
  ( Broadcaster(..)
  , Outflow(..)
  , mailboxId
  , merge
  , outflow
  , outflow_
  ) where

import Orvis.Mailbox (Mailbox, Message(..))
import qualified Orvis.Mailbox as Mailbox
import Orvis.Unique (Id)

data Outflow from =
  forall to. Outflow (Mailbox to)
                     (Message from -> Maybe to)

outflow :: Mailbox to -> (Message from -> Maybe to) -> Outflow from
outflow = Outflow

outflow_ :: Mailbox from -> Outflow from
outflow_ mailbox = Outflow mailbox (pure . message)

mailboxId :: Outflow msg -> Id
mailboxId (Outflow mailbox _) = Mailbox.mailboxId mailbox

sendTo :: Outflow from -> Id -> from -> IO ()
sendTo (Outflow mailbox adapt) i msg =
  let in_ = Mailbox.open mailbox
   in case adapt Message {identity = i, message = msg} of
        Just adaptedMsg ->
          in_ `Mailbox.send` Message {identity = i, message = adaptedMsg}
        Nothing -> return ()

newtype Broadcaster msg = Broadcaster
  { send :: Id -> msg -> IO ()
  }

merge :: [Outflow msg] -> Broadcaster msg
merge [singleOutflow] = Broadcaster $ \i msg -> sendTo singleOutflow i msg
merge outflows =
  Broadcaster $ \i msg -> mapM_ (\out -> sendTo out i msg) outflows
