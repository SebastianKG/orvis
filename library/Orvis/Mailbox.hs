{-# LANGUAGE NamedFieldPuns #-}

module Orvis.Mailbox
  ( Mailbox(mailboxId)
  , Message(..)
  , In
  , Out
  , spawn
  , spawnHidden
  , next
  , open
  , send
  , subscribe
  ) where

import qualified Orvis.Metrics as Metrics
import Orvis.Topology (Topology)
import qualified Orvis.Topology as Topology
import Orvis.Unique (Id)
import Orvis.Unique as Unique

import Control.Concurrent.Chan.Unagi (InChan, OutChan)
import qualified Control.Concurrent.Chan.Unagi as Unagi
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader.Class (ask)

data Mailbox msg = Mailbox
  { mailboxId :: Id
  , broadcastChan :: InChan (Message msg)
  }

data Message msg = Message
  { identity :: Id
  , message :: msg
  }

instance Functor Message where
  fmap f m@Message {message} = m {message = f message}

newtype In msg =
  In (InChan (Message msg))

newtype Out msg =
  Out (OutChan (Message msg))

spawn :: String -> Topology (Mailbox msg)
spawn name = do
  Topology.Tools {Topology.counter, Topology.metrics = tracker} <- ask
  liftIO $ do
    (inChan, _) <- Unagi.newChan
    newId <- Unique.getNext counter
    Metrics.addMailbox
      tracker
      newId
      Metrics.Mailbox {Metrics.mailboxName = name}
    return Mailbox {mailboxId = newId, broadcastChan = inChan}

-- spawn a Mailbox without tracking it in the Topology Metrics
spawnHidden :: Topology (Mailbox msg)
spawnHidden = do
  Topology.Tools {Topology.counter} <- ask
  liftIO $ do
    (inChan, _) <- Unagi.newChan
    newId <- Unique.getNext counter
    return Mailbox {mailboxId = newId, broadcastChan = inChan}

subscribe :: Mailbox msg -> IO (Out msg)
subscribe Mailbox {broadcastChan = broadcast} = do
  myChan <- Unagi.dupChan broadcast
  return $ Out myChan

next :: Out msg -> IO (Message msg)
next (Out chan) = Unagi.readChan chan

open :: Mailbox msg -> In msg
open Mailbox {broadcastChan = broadcast} = In broadcast

send :: In msg -> Message msg -> IO ()
send (In chan) msg = chan `Unagi.writeChan` msg
