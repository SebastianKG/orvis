{-# LANGUAGE NamedFieldPuns #-}

module Orvis.Unique
  ( Counter
  , Id
  , raw
  , getNext
  , spawnCounter
  ) where

import Control.Concurrent.STM.TVar (TVar, newTVar, readTVar, writeTVar)
import Control.Monad.STM (atomically)

newtype Id =
  Id Integer
  deriving (Show, Eq, Ord)

newtype Counter = Counter
  { stored :: TVar Id
  }

spawnCounter :: IO Counter
spawnCounter = do
  counter <- atomically $ newTVar (Id 0)
  return Counter {stored = counter}

getNext :: Counter -> IO Id
getNext Counter {stored} =
  atomically $ do
    myId@(Id i) <- readTVar stored
    writeTVar stored $ Id (i + 1)
    return myId

raw :: Id -> Integer
raw (Id i) = i
